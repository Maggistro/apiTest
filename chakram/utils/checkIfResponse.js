/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const { expect } = require('chakram');
const checkForSuccessfulJsonResponse = require('./checkForSuccessfulJsonResponse');

/**
 * Tests if the response matches
 *
 * @param {object} response
 * @return {object}
 * @throws
 */
function checkIfResponse(response) {
    return {
        ...expect(response),
        isSuccessfulJSONResponse() {
            checkForSuccessfulJsonResponse(response);

            return expect(response);
        },
        isNotSuccessful() {
            expect(response.response.statusCode).to.be.at.least(400);

            return expect(response);
        },
        isNotFoundResponse() {
            expect(response).to.have.status(404);
            expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
            expect(response).not.to.be.encoded.with.gzip;

            return expect(response);
        },
        isNotProcessableResponse() {
            expect(response).to.have.status(422);
            expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
            expect(response).not.to.be.encoded.with.gzip;

            return expect(response);
        },
        hasCollectionWithName(name) {
            const { body } = response;

            expect(body).to.be.an('object');
            expect(body).to.have.property(name);
            expect(body[name]).to.be.an('array');

            return {
                ...expect(body[name]),
                withLength(length) {
                    expect(body[name]).to.have.lengthOf(length);

                    return expect(body[name]);
                },
            };
        },
    };
}

module.exports = checkIfResponse;
