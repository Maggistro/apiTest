/**
 * Created on 31.10.18.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

module.exports = {
    list: '/users',
    add: '/users',
    get: '/users/{id}',
    edit: '/users/{id}',
    destroy: '/users/{id}',
};
